package net.pirsquare.epson;

import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.os.Handler;
import android.widget.Toast;

import com.epson.eposprint.BatteryStatusChangeEventListener;
import com.epson.eposprint.EposException;
import com.epson.eposprint.Print;
import com.epson.eposprint.StatusChangeEventListener;
import com.epson.epsonio.DevType;
import com.epson.epsonio.DeviceInfo;
import com.epson.epsonio.EpsonIoException;
import com.epson.epsonio.FilterOption;
import com.epson.epsonio.Finder;
import com.epson.epsonio.IoStatus;
import org.jdeferred.Deferred;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

public class DiscoverPrinter implements Runnable, StatusChangeEventListener, BatteryStatusChangeEventListener {

    final static int DISCOVERY_INTERVAL = 500;

    ScheduledExecutorService scheduler;
    ScheduledFuture<?> future;
    Handler handler = new Handler();
    Print printer;
    int dev_type;
    private Activity mActivity;

    public String dev_name = "";
    public String dev_addr = "";

    private DiscoverPrinter mDiscoverPrinter;

    public DiscoverPrinter(Activity a) {
        mActivity = a;
        mDiscoverPrinter = this;
    }

    public Promise promise;
    private Deferred d;

    public Promise findUSBDDevice() {
        d = new DeferredObject();
        promise = d.promise();

        //stopFind();

        //start find thread scheduler
        if (scheduler == null)
            scheduler = Executors.newSingleThreadScheduledExecutor();

        //find start
        dev_type = DevType.USB;
        findStart();
        return promise;
    }

    public Promise findTCPDevice() {
        d = new DeferredObject();
        promise = d.promise();

        //stopFind();

        //start find thread scheduler
        if (scheduler == null)
            scheduler = Executors.newSingleThreadScheduledExecutor();

        //find start
        dev_type = DevType.TCP;
        findStart();
        return promise;
    }


    public void dispose() {
        //stop find
        if (future != null) {
            future.cancel(false);
            while (!future.isDone()) {
                try {
                    Thread.sleep(DISCOVERY_INTERVAL);
                } catch (Exception e) {
                    break;
                }
            }
            future = null;
        }
        if (scheduler != null) {
            scheduler.shutdown();
            scheduler = null;
        }
        //stop old finder
        while (true) {
            try {
                Finder.stop();
                break;
            } catch (EpsonIoException e) {
                if (e.getStatus() != IoStatus.ERR_PROCESSING) {
                    break;
                }
            }
        }

        try {
            printer.closePrinter();
            printer = null;
        } catch (EposException e) {
            e.printStackTrace();
        }
    }

    @Override
    //find thread
    public synchronized void run() {
        class UpdateListThread extends Thread {
            DeviceInfo[] list;

            public UpdateListThread(DeviceInfo[] listDevices) {
                list = listDevices;
            }

            @Override
            public void run() {
                if (!d.isPending()) {
                    stopFind();
                    return;
                }
                if (list != null&&list.length>0) {
                    //for (int i = 0; i < list.length; i++) {
                    dev_name = list[0].getPrinterName();
                    dev_addr = list[0].getDeviceName();

                    String msg = "PRINTER FOUND: " + dev_addr + " NAME: " + dev_name;
                    Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();

                    try {
                        printer = new Print(mActivity.getApplicationContext());
                        printer.setStatusChangeEventCallback(mDiscoverPrinter);
                        printer.setBatteryStatusChangeEventCallback(mDiscoverPrinter);
                        if (dev_type == DevType.USB)
                            printer.openPrinter(Print.DEVTYPE_USB, dev_addr, Print.FALSE, 1000);
                        if (dev_type == DevType.TCP)
                            printer.openPrinter(Print.DEVTYPE_TCP, dev_addr, Print.FALSE, 1000);

                        d.resolve(printer);
                    } catch (EposException e) {
                        e.printStackTrace();
                        d.reject(e);
                    }

                    stopFind();
                    // }
                }
            }
        }

        DeviceInfo[] deviceList = null;
        try {
            deviceList = Finder.getDeviceInfoList(FilterOption.PARAM_DEFAULT);
            handler.post(new UpdateListThread(deviceList));
        } catch (Exception e) {
            return;
        }
    }

    public void stopFind() {

        //stop old finder
        while (true) {
            try {
                Finder.stop();
                break;
            } catch (EpsonIoException e) {
                if (e.getStatus() != IoStatus.ERR_PROCESSING) {
                    break;
                }
            } catch (ExceptionInInitializerError e) {
                e.printStackTrace();
                break;
            }
        }

        //stop find thread
        if (future != null) {
            future.cancel(false);
            while (!future.isDone()) {
                try {
                    Thread.sleep(DISCOVERY_INTERVAL);
                } catch (Exception e) {
                    break;
                }
            }
            future = null;
        }
    }

    //find start/restart
    private void findStart() {
        if (scheduler == null) {
            return;
        }

        stopFind();

        try {
            if (dev_type == DevType.TCP)
                Finder.start(mActivity, dev_type, "255.255.255.255");
            else
                Finder.start(mActivity, dev_type, null);
        } catch (Exception e) {
            Toast.makeText(mActivity, "Finder Error: " + e.toString(), Toast.LENGTH_SHORT).show();
            return;
        }

        //start thread
        future = scheduler.scheduleWithFixedDelay(this, 0, DISCOVERY_INTERVAL, TimeUnit.MILLISECONDS);
    }

    @Override
    public void onStatusChangeEvent(final String deviceName, final int status) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mActivity, "StatusChange: " + status, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public void onBatteryStatusChangeEvent(final String deviceName, final int battery) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mActivity, "Battery: " + battery, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
