package net.pirsquare.epson;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.epson.eposprint.Builder;
import com.epson.eposprint.EposException;
import com.epson.eposprint.Print;

/**
 * Created by Michael on 05/11/2014.
 */
public class PrinterHelper {
    private static final int IMAGE_WIDTH_MAX = 512;
    private Builder b;
    private Print p;
    private Context mContext;

    /**
     * @param model int R.string.printername_t88v
     * @param lang  int
     * @param c     Context
     * @return
     */
    public PrinterHelper(Print print, int model, int lang, Context c) throws EposException {
        mContext = c;
        p = print;
        b = new Builder(c.getString(model), lang, c);
    }

    /**
     * @param b
     * @param font       Builder.FONT_A
     * @param line_space int
     * @param lang       Builder.LANG_EN
     * @param size_x     int
     * @param size_y     int
     * @param underline  Builder.TRUE/FALSE
     * @param bold       Builder.TRUE/FALSE
     * @param color      Builder.COLOR_1
     * @param pos_x      int
     * @param text       String
     * @return
     */
    public PrinterHelper addText(int font, int line_space, int lang, int size_x, int size_y, int underline, int bold, int color, int pos_x, String text) {
        try {
            b.addTextFont(font);
            b.addTextLineSpace(line_space);
            b.addTextLang(lang);
            b.addTextSize(size_x, size_y);
            b.addTextStyle(Builder.FALSE, underline, bold, color);
            b.addTextPosition(pos_x);
            b.addText(text);
        } catch (EposException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    /**
     * @param align Builder.ALIGN_LEFT
     * @return
     */
    public PrinterHelper alignElement(int align) {
        try {
            b.addTextAlign(align);
        } catch (EposException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    /**
     * @param bitmap
     * @param pos_x      int
     * @param pos_y      int
     * @param width      int
     * @param height     int
     * @param color      Builder.COLOR_1
     * @param mode       Builder.MODE_GRAY16 / Builder.MODE_MONO
     * @param tone       Builder.HALFTONE_ERROR_DIFFUSION / Builder.HALFTONE_THRESHOLD / Builder.HALFTONE_DITHER
     * @param brightness int default = 1
     * @return
     */
    public PrinterHelper addBitmap(Bitmap bitmap, int pos_x, int pos_y, int width, int height, int color, int mode, int tone, int brightness) {
        try {
            b.addImage(bitmap,
                    0,
                    0,
                    Math.min(IMAGE_WIDTH_MAX, width),
                    height,
                    color,
                    mode,
                    tone,
                    brightness,
                    Builder.COMPRESS_DEFLATE);
        } catch (EposException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    /**
     * @param bitmap
     * @return
     */
    public PrinterHelper addBitmap(Bitmap bitmap) {
        return addBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), Builder.COLOR_1, Builder.MODE_GRAY16, Builder.HALFTONE_ERROR_DIFFUSION, 1);
    }

    /**
     * @param bitmap
     * @return
     */
    public PrinterHelper addBitmapResource(int id) {
        Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), id);
        PrinterHelper ret = addBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), Builder.COLOR_1, Builder.MODE_GRAY16, Builder.HALFTONE_ERROR_DIFFUSION, 1);

        bitmap = null;
        return ret;
    }

    /**
     * Print at 100x100 level-L
     *
     * @param data
     * @return
     */
    public PrinterHelper addQR(String data) {
        return addQR(data, Builder.LEVEL_L, 100);
    }

    /**
     * @param data   String
     * @param level  Builder.LEVEL_L ..
     * @param size_x int
     * @param size_y int
     * @return
     */
    public PrinterHelper addQR(String data, int level, int size) {
        try {
            b.addSymbol(
                    data,
                    Builder.SYMBOL_QRCODE_MODEL_2,
                    level,
                    100,
                    100,
                    size);
        } catch (EposException e) {
            e.printStackTrace();
            return null;
        }

        return this;
    }

    /**
     * @param cutType int
     *                Builder.CUT_NO_FEED/Builder.CUT_FEED/Builder.CUT_RESERVE
     * @return
     */
    public PrinterHelper addCut(int cut_type) {
        try {
            b.addCut(cut_type);
        } catch (EposException e) {
            e.printStackTrace();
            return null;
        }

        return this;
    }

    public PrinterHelper feedLine(int f) {
        try {
            b.addFeedLine(f);
        } catch (EposException e) {
            e.printStackTrace();
            return null;
        }

        return this;
    }

    public PrinterHelper feedUnit(int f) {
        try {
            b.addFeedUnit(f);
        } catch (EposException e) {
            e.printStackTrace();
            return null;
        }

        return this;
    }

    /**
     * Builder.FEED_CURRENT_TOF/Builder.FEED_CUTTING/Builder.FEED_NEXT_TOF/Builder.FEED_PEELING
     * @param f
     * @return
     */
    public PrinterHelper addFeedPosition(int f) {
        try {
            b.addFeedPosition(f);
        } catch (EposException e) {
            e.printStackTrace();
            return null;
        }

        return this;
    }

    public PrinterHelper print() {
        int[] status = new int[1];
        int[] battery = new int[1];
        try {
            p.sendData(b, 10000, status, battery);
        } catch (EposException e) {
            e.printStackTrace();
            return null;
        }

        return this;
    }

    public PrinterHelper dispose(){
        b.clearCommandBuffer();
        b=null;
        mContext=null;
        return this;
    }

}
