package net.pirsquare.file;

import android.util.Log;

import java.io.*;

/**
 * Created by Michael on 31/10/2014.
 */
public class IOUtil {
    private static String TAG = IOUtil.class.getCanonicalName();

    public static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
	     * method. We iterate until the BufferedReader return null which means
	     * there's no more data to read. Each line will appended to a StringBuilder
	     * and returned as String.
	     */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString().substring(0,sb.length()-1);
    }

    public static String loadStringFromFile(File file) {
        return new String(loadByteFromFile(file));
    }

    public static byte[] loadByteFromFile(File file) {
        if (file == null)
            return null;

        if (!file.exists())
            return null;

        int length = (int) file.length();
        byte[] bytes = new byte[length];

        try {
            FileInputStream in = new FileInputStream(file);
            in.read(bytes);
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
        }

        return bytes;
    }

    public static void SaveBytes(File file, byte[] bs) {
        try {
            if (file.exists()) {
//	            	delete if have older.
                file.delete();
            }
            file.createNewFile();

//	            create output stream
            FileOutputStream fos = new FileOutputStream(file);
//	            get byte total

            fos.write(bs, 0, bs.length);
//	            check user press cancel from progress dialog.
//	            close stream
            fos.flush();
            fos.close();

        } catch (Exception e) {
            Log.e(TAG, "save file error! " + e.toString());
        }
    }
}
