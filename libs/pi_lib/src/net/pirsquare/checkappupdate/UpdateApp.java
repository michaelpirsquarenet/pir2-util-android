package net.pirsquare.checkappupdate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.View;

public class UpdateApp extends AsyncTask<String, Void, Void> {

    private UpdateApp _constance;
    private Activity _activity;

    //paths
    private String _downloadURI;
    private String _savePath;
    private String _fileName;
    private String _appName;
    private String _brand;

    public UpdateApp(Activity mActivity, String downloadURI, String savePath, String fileName, String appName, String brand) {
        _activity = mActivity;
        _constance = this;

        _downloadURI = downloadURI;
        _savePath = savePath;
        _fileName = fileName;

        File savePath_dic = new File(savePath);
        if (!savePath_dic.exists())
            savePath_dic.mkdirs();

        _appName = appName;
        _brand = brand;
    }

    private void showErrorDialog() {
        View v = _activity.getWindow().getDecorView().findViewById(android.R.id.content);
        v.post(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(_appName);
                builder.setMessage("Connection error ไม่สามารถติดต่อเซิร์ฟเวอร์ เพื่อทำการ อัพเดท")
                        .setPositiveButton("ปิด", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // FIRE ZE MISSILES!
                            }
                        });

                // Create the AlertDialog object and return it
                final AlertDialog alert = builder.create();
                alert.show();

                dismissDelay(alert);
            }
        });

    }

    private void removeCheckVersionDialog() {
        if (barProgressDialog != null) {
            barProgressDialog.dismiss();
            barProgressDialog = null;
        }
    }

    private void showCheckVersionProgress() {
        barProgressDialog = new ProgressDialog(_activity);
        barProgressDialog.setTitle(_appName);
        barProgressDialog.setMessage("Checking version.");
        barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        barProgressDialog.setCancelable(false);
        barProgressDialog.show();
    }

    private String getDeviceName() {
        return android.os.Build.MODEL;
    }

    private String getWiFiMacAddress() {

        WifiManager manager = (WifiManager) _activity.getSystemService(Context.WIFI_SERVICE);
        manager.setWifiEnabled(true);
        WifiInfo info = manager.getConnectionInfo();
        if (info == null)
            return "";
        else
            return info.getMacAddress();
    }

    private String getBluetoothMacAddress() {

        BluetoothAdapter bluetoothAdepter = null;

        bluetoothAdepter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdepter == null)
            return "";
        else
            return bluetoothAdepter.getAddress();
    }

    public String result_str;

    public void checkUpdate(String _version, final Boolean showDialog, Boolean noupdateIsShowMsg, final Boolean shownoti) {
//		check show dialog
        if (showDialog)
            showCheckVersionProgress();

//		keep version compare in thread.
        final String mVersion = _version;
        final Boolean noUpdateShowDialog = noupdateIsShowMsg;
//		Create check version Thread in background
        new Thread(new Runnable() {

            @Override
            public void run() {
//				encode version
                String version = "";
                String device_name = getDeviceName();
                String wifi_mad = getWiFiMacAddress();
                String bluetooth_mad = getBluetoothMacAddress();
                String time = new Date().toString();

                try {
                    device_name = URLEncoder.encode(device_name, "UTF-8");
                    version = URLEncoder.encode(mVersion, "UTF-8");
                    wifi_mad = URLEncoder.encode(wifi_mad, "UTF-8");
                    bluetooth_mad = URLEncoder.encode(bluetooth_mad, "UTF-8");
                    time = URLEncoder.encode(time, "UTF-8");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

                //String url = "http://www.pirsquare.net/project/qhappy/service/api/atv/version?"+"version="+version+"&device_name="+device_name+"&wifi_mad="+wifi_mad+"&bluetooth_mad="+bluetooth_mad+"&time="+time;

//				Create Connextion
                HttpClient httpclient = new DefaultHttpClient();

                // Prepare a request object
                HttpGet httpget = new HttpGet(_downloadURI + "version=" + version + "&device_name=" + device_name + "&wifi_mad=" + wifi_mad + "&bluetooth_mad=" + bluetooth_mad + "&time=" + time + "&brand=" + _brand);

                // Execute the request
                HttpResponse response;
                try {
                    response = httpclient.execute(httpget);
                    // Examine the response status
                    //Log.i("Praeda", response.getStatusLine().toString());

                    // Get hold of the response entity
                    HttpEntity entity = response.getEntity();
                    // If the response does not enclose an entity, there is no need
                    // to worry about connection release

                    if (entity != null) {
                        // A Simple JSON Response Read
                        InputStream instream = entity.getContent();
//			            remove dialog if have.
                        removeCheckVersionDialog();
//						get result from stream.
                        String result_stream = convertStreamToString(instream);
//			            decode JSON String
                        JSONObject jsonObj = new JSONObject(result_stream);
//			            get result from node.
                        result_str = jsonObj.getString("result");
//			            get root view
                        View v = _activity.getWindow().getDecorView().findViewById(android.R.id.content);
//			            compare result is have update.

                        if (result_str.equals("no_update")) {
//			            call main thread.
                            if (noUpdateShowDialog == true)
                                v.post(new Runnable() {
                                    @Override
                                    public void run() {
//			                           	create no update dialog.
                                        AlertDialog.Builder builder = new AlertDialog.Builder(_activity);
                                        builder.setCancelable(true);
                                        builder.setTitle(_appName);
                                        builder.setMessage("No Update");
                                        builder.setInverseBackgroundForced(true);
                                        builder.setPositiveButton("ปิด",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog,
                                                                        int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                        AlertDialog alert = builder.create();
                                        alert.show();

                                        dismissDelay(alert);

                                    }
                                });
                        } else {

//				            call main thread.
                            v.post(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setTitle(_appName);

                                    builder.setMessage("กรุณาอัปเดท " + _appName + " เป็นเวอร์ชั่นล่าสุด เพื่อการทำงานที่ถูกต้อง")
                                            .setPositiveButton("ปิด", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {

                                                }
                                            })
                                            .setNegativeButton("อัพเดท", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    onClickUpdate();
                                                }
                                            });

                                    // Create the AlertDialog object and return it

                                    final AlertDialog alert = builder.create();
                                    alert.show();

                                    dismissDelay(alert);

                                    //									show progress dialog.
                                }
                            });
//			            	start download update file from url.

                        }

                        instream.close();
                    }

                } catch (Exception e) {
//			    	throw error.
                    Log.d("", e.toString());
                    removeCheckVersionDialog();

                    showErrorDialog();
                }
            }
        }).start();
    }

    public void dismissDelay(final AlertDialog alert) {
        Runnable _runnable = new Runnable() {
            @Override
            public void run() {
                alert.dismiss();
            }
        };
        //auto close
        new Handler().postDelayed(_runnable, 25000);
    }

    public void onClickUpdate() {
        showDialog();
        _constance.execute(result_str);
    }

    public void onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action.equals("clickUpdate")) onClickUpdate();
    }

    private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
	     * method. We iterate until the BufferedReader return null which means
	     * there's no more data to read. Each line will appended to a StringBuilder
	     * and returned as String.
	     */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    protected Void doInBackground(String... params) {
        try {
//			create connection
            URL url = new URL(params[0]);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestProperty("Accept-Encoding", "identity");
            c.connect();
//            set save file to local path.
            //String PATH = "/mnt/sdcard/Download/";
            File file = new File(_savePath);
            file.mkdirs();
            File outputFile = new File(file, _fileName);
//            check old file download
            if (outputFile.exists()) {
//            	delete if have older.
                outputFile.delete();
            }

//            create output stream
            final FileOutputStream fos = new FileOutputStream(outputFile);
//            get byte total
            long contentLength = c.getContentLength();
            Log.d("Byte total ", "" + contentLength);

            InputStream is = c.getInputStream();
            barProgressDialog.setMax(100);
//            Loop get byte
            byte[] buffer = new byte[8192];
            int total = 0;
            int count = 0;
            int current = 0;
            while ((count = is.read(buffer)) != -1) {
                total += count;
                current = Integer.parseInt("" + (int) (total * 100 / contentLength));
                barProgressDialog.setProgress(current);

                fos.write(buffer, 0, count);
//                check user press cancel from progress dialog.
                if (_constance.isCancelled())
                    throw new Exception("Cancel Task.");
            }
//            close stream
            fos.flush();
            fos.close();
            is.close();

//            close progress dialog if have
            if (barProgressDialog != null) {
                barProgressDialog.dismiss();
                barProgressDialog = null;
            }

//            start install Activity
            Intent intent = new Intent(Intent.ACTION_VIEW);
            //intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/update.apk")), "application/vnd.android.package-archive");
            intent.setDataAndType(Uri.fromFile(new File(_savePath + _fileName)), "application/vnd.android.package-archive");

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
            _activity.startActivity(intent);

        } catch (Exception e) {

            Log.e("UpdateAPP", "Update error! " + e.getMessage());
            removeCheckVersionDialog();
            if (!_constance.isCancelled())
                showErrorDialog();
        }

        return null;
    }

    private void createNotificationProgress() {

    }

    private ProgressDialog barProgressDialog;

    public void showDialog() {
        barProgressDialog = new ProgressDialog(_activity);
        barProgressDialog.setTitle(_appName);
        barProgressDialog.setCancelable(false);
        barProgressDialog.setMessage("New Installer Downloading...");
        barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);


        barProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                barProgressDialog = null;
                _constance.cancel(true);
            }
        });

        barProgressDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Hide", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // barProgressDialog.dismiss();
                // barProgressDialog = null;

                createNotificationProgress();
            }
        });

        barProgressDialog.show();
    }

    public Activity getActivity() {
        return _activity;
    }

    public void setActivity(Activity activity) {
        this._activity = activity;
    }

}
