package net.pirsquare.logger;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import android.os.Handler;
import android.util.Log;

public class SocketLogger {

    private WebSocketClient _wsc = null;
    private String _branch_id;
    private String _serverUri;
    private Draft _draft;
    private final String TAG = "SOCKET";

    public boolean autoReconnect = true;

    public SocketLogger(String serverUri, Draft draft,String branch_id) throws URISyntaxException {
        _branch_id=branch_id;
        _serverUri=serverUri;
        _draft=draft;
        Log.i(TAG, "SocketLogger: "+serverUri + " branch_id: "+branch_id);

        r.run();
    }
    private List<String> pendingString = new ArrayList<String>();
    private Handler h = new Handler();
    private Runnable r = new Runnable() {
        @Override
        public void run() {

            if(_wsc!=null)
                if(!_wsc.isClosed())
                    _wsc.close();

            _wsc=null;
            try {
                _wsc = new WebSocketClient(new URI(_serverUri),_draft) {

                    public boolean isDead=false;
                    @Override
                    public String getRoom(){
                        return "";
                    }
                    @Override
                    public void setRoom(String r){

                    }

                    @Override
                    public void connect() {
                        if(isOpen())
                            return;
                        else{
                            Log.i(TAG, "connect: ");
                            super.connect();
                        }
                    }

                    @Override
                    public void onOpen(ServerHandshake handshakedata) {
                        this.send("sender@"+_branch_id);
                        Log.i(TAG, "onOpen: "+handshakedata.toString());
                        isDead=false;
                    }

                    @Override
                    public void onMessage(String message) {

                    }


                    @Override
                    public void onError(Exception ex) {
                        Log.i(TAG, "onError: "+ex);
                        if(isDead)return;
                        this.close();
                    }
                    @Override
                    public void onClose(int code, String reason, boolean remote) {
                        Log.i(TAG, "onClose: "+code+" | "+reason+" | "+remote);

                        if(isDead)return;

                        retryConnect();

                        isDead=true;
                    }

                    @Override
                    public void send(String text) {
                        if(isOpen()){
                            Iterator<String> iter = pendingString.iterator();
                            while(iter.hasNext()){
                                String message = iter.next();
                                super.send(message);
                            }
                            pendingString.clear();

                            super.send(text);
                        }else{
                            pendingString.add(text);
                        }
                    }
                };
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            _wsc.connect();
        }
    };

    private void retryConnect(){
        if(autoReconnect){
            h.postDelayed(r, 5000);
        }
    }

    public void connect(){
        if(_wsc!=null)
            _wsc.connect();
    }

    public void close(){
        if(_wsc!=null)
            _wsc.close();
        _wsc=null;
        autoReconnect=false;
    }

    public void send(String message){
        if(_wsc!=null)
            _wsc.send(message);
    }




}
