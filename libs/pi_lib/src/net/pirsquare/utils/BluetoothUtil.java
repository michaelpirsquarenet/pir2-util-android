package net.pirsquare.utils;

import android.bluetooth.BluetoothAdapter;
import android.util.Log;

/**
 * Created by katopz on 2014-10-21.
 */
public class BluetoothUtil {

    private static String TAG = "BluetoothUtil";

    public static String getBluetoothAddress()
    {
        String deviceAddress = "";

        try
        {
            deviceAddress = BluetoothAdapter.getDefaultAdapter().getAddress();
        } catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

        return deviceAddress;
    }
}
