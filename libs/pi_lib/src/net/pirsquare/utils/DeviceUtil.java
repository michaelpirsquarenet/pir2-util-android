package net.pirsquare.utils;

import android.app.Activity;
import android.content.Context;
import android.os.PowerManager;
import android.view.Window;

/**
 * Created by katopz on 2014-10-27.
 */
public class DeviceUtil {
    public static void keepScreenOn(Window window,Activity activity)
    {
        window.addFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        final PowerManager pm = (PowerManager) activity.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE,"");
        mWakeLock.acquire();
    }
}
