package net.pirsquare.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import org.jdeferred.Deferred;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.util.ArrayList;

public class SQLStore extends SQLiteOpenHelper {

    private SQLiteDatabase _db;
    private Boolean _isInit = false;
    private String _dbName = "null";

    private SQLiteDatabase DBW = null;
    private SQLiteDatabase DBR = null;

    public Boolean getIsInit() {
        return _isInit;
    }

    public SQLStore(Context c, String name) {
        super(c, name, null, 1600000);
        _dbName = name;
        //Log.i("ANDROID_SQL", "SQLStore");
    }

    public SQLStore(Context c, String name, int version) {
        super(c, name, null, version);
        _dbName = name;
        //Log.i("ANDROID_SQL", "SQLStore");
    }

    public void open() {
        if (!_isInit) {
            DBR = getReadableDatabase();
            DBR.setPageSize(8192);
            DBW = getWritableDatabase();
            DBW.setPageSize(8192);
        }
    }

    public void openReadOnly() {
        if (!_isInit) {
            DBR = getReadableDatabase();
            DBR.setPageSize(8192);
        }
    }

    public void close() {
        _db.close();
        _isInit = false;
        if (DBW != null)
            DBW.close();
        if (DBR != null)
            DBR.close();
        DBW = DBR = null;
        writeQueue.clear();
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        _db = db;
        //Log.i("ANDROID_SQL", "onOpen");
        _isInit = true;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        _db = db;
        //Log.i("ANDROID_SQL", "onCreate");
        _isInit = true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Log.i("ANDROID_SQL", "onUpgrade oldv:" + oldVersion + "  newv:" + newVersion);
    }

    public void executeWrite(String statement) {
        //Log.i("ANDROID_SQL", "executeSyncData:" + statement);

        writeQueue.add(statement);
        executeWriteQueue();
    }

    public Promise waitForWriteDone() {
        return executeWriteQueue();
    }

    private ArrayList<String> writeQueue = new ArrayList<String>();
    private boolean isExecuteWriteQueue = false;

    public boolean isWriting() {
        final boolean b = isExecuteWriteQueue;
        return b;
    }

    private Promise executeWriteQueue() {
        final Deferred deferred = new DeferredObject();
        Promise promise = deferred.promise();

        if (writeQueue.size() <= 0) {
            deferred.resolve(null);
            return promise;
        }

        isExecuteWriteQueue = true;

        while (writeQueue.size() > 0) {
            if (!_db.isDbLockedByCurrentThread()) {
                try {
                    DBW.execSQL(writeQueue.remove(0));
                }catch (Exception e){

                }
            }
        }

        isExecuteWriteQueue = false;
        deferred.resolve(null);
        return promise;
    }

    public Cursor executeRead(String statement) {
        if (_db.inTransaction())
            throw new Error("Allow only one executeSyncData");

        //Log.i("ANDROID_SQL", "executeSyncData:" + statement);
        return DBR.rawQuery(statement, null);
    }


}
