package net.pirsquare.system;

import java.util.Date;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;
import android.widget.EditText;
import android.widget.Toast;

public class PiSystem {
    public static void RESTARTAPP(Activity activity) {
        Context context = activity.getApplicationContext();
        Class<?> clazz = activity.getClass();

        Intent mStartActivity = new Intent(context, clazz);
        int mPendingIntentId = (int) (new Date().getTime() * .1);

        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, mPendingIntent);
        System.exit(0);
    }

    public static void exit() {
        System.exit(0);
    }

    public static void SHOWDIALOGUE(Activity _activity, String title, String message) {
        SHOWDIALOGUE(_activity, title, message, null);
    }

    public static void SHOWDIALOGUE(Activity _activity, String title, String message, final Runnable callBack) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(_activity);
        builder1.setTitle(title);
        builder1.setMessage(message);
        builder1.setCancelable(false);
        builder1.setNeutralButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        if (callBack != null)
                            callBack.run();
                    }
                });

        AlertDialog alert = builder1.create();
        alert.show();
    }

//	public static void SHOWDIALOGUEYESNO(Activity _activity,String title,String message,String yes,String no) {
//		 AlertDialog.Builder builder1 = new AlertDialog.Builder(_activity);
//			builder1.setTitle(title);
//			builder1.setMessage(message);
//			builder1.setCancelable(false);
//			builder1.setPositiveButton(yes,
//			        new DialogInterface.OnClickListener() {
//			    public void onClick(DialogInterface dialog, int id) {
//			    	FREC.dispatchStatusEventAsync("SHOWDIALOGUEYESNO", "yes");
//			    }}).setNegativeButton(no,
//				        new DialogInterface.OnClickListener() {
//				    public void onClick(DialogInterface dialog, int id) {
//				    	FREC.dispatchStatusEventAsync("SHOWDIALOGUEYESNO", "no");
//				    }});
//
//			AlertDialog alert = builder1.create();
//			alert.show();
//	}
//
//	public static void SHOWINPUTDIALOG(Activity _activity,String title,String message,String yes,String no,String text) {
//		 AlertDialog.Builder builder1 = new AlertDialog.Builder(_activity);
//			builder1.setTitle(title);
//			builder1.setMessage(message);
//			builder1.setCancelable(false);
//			final EditText et =new EditText(_activity);
//			et.setText(text);
//			builder1.setView(et);
//
//			builder1.setPositiveButton(yes,
//			        new DialogInterface.OnClickListener() {
//			    public void onClick(DialogInterface dialog, int id) {
//			    	FREC.dispatchStatusEventAsync("SHOWINPUTDIALOG","{\"method\":\"yes\",\"value\":\"" + et.getText().toString() + "\"}");
//			    }}).setNegativeButton(no,
//			        new DialogInterface.OnClickListener() {
//			    public void onClick(DialogInterface dialog, int id) {
//			    	FREC.dispatchStatusEventAsync("SHOWINPUTDIALOG", "{\"method\":\"no\"}");
//			    }});
//
//			AlertDialog alert = builder1.create();
//			alert.show();
//	}

//	public static void SHOWDIALOG_OK(Activity _activity,String title,String message) {
//		 AlertDialog.Builder builder1 = new AlertDialog.Builder(_activity);
//			builder1.setTitle(title);
//			builder1.setMessage(message);
//			builder1.setCancelable(false);
//
//			builder1.setNeutralButton(android.R.string.ok,
//			        new DialogInterface.OnClickListener() {
//			    public void onClick(DialogInterface dialog, int id) {
//			        dialog.cancel();
//			        FREC.dispatchStatusEventAsync("SHOWDIALOG_OK", "{\"method\":\"ok\"}");
//			    }
//			});
//
//			AlertDialog alert = builder1.create();
//			alert.show();
//	}

    public static void SHOWNOTIFICATION(Activity activity, String header, String message, Boolean onGoing, int icon, boolean sound_noti) {
        Context context = activity.getApplicationContext();
        Class<?> clazz = activity.getClass();

        Intent intent = new Intent(activity, clazz);

        //PendingIntent.FLAG_CANCEL_CURRENT will bring the app back up again
        PendingIntent pIntent = PendingIntent.getActivity(context, PendingIntent.FLAG_CANCEL_CURRENT, intent, 0);
        Notification mNotification = null;
        // define sound URI, the sound to be played when there's a notification
        if (sound_noti) {
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mNotification = new Notification.Builder(activity)
                    .setContentTitle(header)
                    .setContentText(message)
                    .setSmallIcon(icon)
                    .setContentIntent(pIntent)
                    .setOngoing(onGoing)//optional
                    .setSound(soundUri)
                    .build();
        }else{
            mNotification = new Notification.Builder(activity)
                    .setContentTitle(header)
                    .setContentText(message)
                    .setSmallIcon(icon)
                    .setContentIntent(pIntent)
                    .setOngoing(onGoing)//optional
                    .build();
        }

        NotificationManager notificationManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, mNotification);
    }

    public static void SHOW_TOAST(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    private static Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    private static Ringtone r;

    public static void SOUNDNOTI(Context c) {
        if (r == null) {
            r = RingtoneManager.getRingtone(c, notification);
        }
        if (r.isPlaying())
            r.stop();
        r.play();
    }


    public static Boolean ISDISPLAYON(Activity a) {
        PowerManager powerManager = (PowerManager) a.getSystemService(Context.POWER_SERVICE);
        return powerManager.isScreenOn();
    }
}
